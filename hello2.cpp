///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello C++
///
/// @file hello1.cpp
/// @version 1.0
///
/// A 'Hello World' program written using C++
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 04c - Hello C++ - EE 205 - Spr 2021
/// @date 09_FEB_2021 
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

int main() {

   std::cout << "Hello World!" << std::endl;

   return 0;
}

